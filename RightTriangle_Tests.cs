﻿using System;
using NUnit.Framework;
using Triangle;

namespace RightTriangle_Tests
{
    [TestFixture]
    class RightTriangle_Tests
    {
        [TestCase(1, 2, 3, 1)]
        [TestCase(3, 2, 1, 1)]
        [TestCase(1, 0, 2, 0)]
        [TestCase(1, 1, 1, 0.5)]
        public void Area_ThreeSides_RightResult(double a, double b, double c, double result)
        {
            RightTriangle triangle = GetRightTriangle();
            double area = triangle.Area(a, b, c);

            Assert.IsTrue(area == result);
        }

        [TestCase(-1, 2, 3)]
        [TestCase(3, -2, 1)]
        [TestCase(1, 0, -2)]
        public void Area_LessThenZroSide_ArgumentException(double a, double b, double c)
        {
            RightTriangle triangle = GetRightTriangle();
            Assert.Throws<ArgumentException>(() => {
                triangle.Area(a, b, c);
            });
        }

        [TestCase(1, 10, 5)]
        [TestCase(2, 10, 10)]
        public void Area_ByTwoSides_RightResult(double a, double b, double result)
        {
            RightTriangle triangle = GetRightTriangle();
            double area = triangle.Area(a, b);

            Assert.IsTrue(area == result);
        }


        private RightTriangle GetRightTriangle()
        {
            return new RightTriangle();
        }
    }
}
